const {defaults} = require('jest-config')

module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  moduleNameMapper: {
    '\\.(css|styl|less|sass|scss)$': '<rootDir>/__mocks__/styleMock.js',
    '#': '<rootDir>/types',
    '<rootDir>\\/node_modules\\/vuetify\\/.*$': '<rootDir>/__mocks__/styleMock.js',
  },
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.(ts|tsx)$': 'ts-jest',
    '^.+\\.(mjs|js|jsx)$': 'babel-jest',
    '<rootDir>\\/node_modules\\/vuetify\\/.*$': 'babel-jest',
  },
  transformIgnorePatterns: [
    '<rootDir>\\/node_modules(?!\\/leaflet)\\/.*$',
    '<rootDir>\\/node_modules\\/vuetify\\/.*$',
    '\\.pnp\\.[^\\\/]+$',
  ],
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx', 'mjs'],
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{ts,js,vue}',
    '!**/node_modules/**',
    '!src/main.ts',
    '!src/App.vue',
    '!src/plugins/*.ts',
  ],
}
