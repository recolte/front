export interface LocaleAvailable {
    id: number;
    locale: string;
}

export interface Picture {
    alternativeText: string;
    id: number;
    url: string;
}

export interface Vegetable {
    id: number;
    name: string;
    locale: string;
    picture: Picture;
    localizations: LocaleAvailable[]
}
