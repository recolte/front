export interface ChartData {
    category: string;
    y1: number;
    y2: number;
    y3: number;
}

export interface Coordinates {
    lat: number;
    lng: number;
    surface: number;
    unit: 'm' | 'a' | 'ha';
}

export interface Marker {
    lat: number;
    lng: number;
    id: string;
    address: string;
    surface: {
        value: number | null;
        unit: 'm' | 'a' | 'ha' | null;
    };
}

export interface OSMReverseResponse {
    address: {
        allotments?: string;
        amenity?: string;
        city?: string;
        country: string;
        country_code: string;
        county?: string;
        house_number?: string;
        leisure?: string;
        municipality?: string;
        postcode?: string;
        road?: string;
        state?: string;
        suburb?: string;
        town?: string;
        village?: string;
    };
    boundingbox: string[];
    display_name: string;
    lat: string;
    licence: string;
    lon: string;
    osm_id: number;
    osm_type: string;
    place_id: number;
}
