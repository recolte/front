export interface Harvest {
    id: number;
    number: number | null;
    weight: number | null;
    user: number;
    vegetable: {
        id: number;
        name: string;
        picture: {
            alternativeText: string;
            id: number;
            url: string;
        };
    };
}
