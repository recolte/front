import './Map'
import './Security'
import './Vegetable'

export interface InputItemFormat {
    text: string;
    value: string;
    id?: string;
    children?: InputItemFormat[];
}
