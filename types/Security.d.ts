import {Coordinates} from './Map';

export interface RoleUser {
    id: number;
    name: string;
    type: string;
}

export interface SurfaceUser {
    value: number | null;
    unit: number | string;
}

export interface ProductionUser {
    value: string | number;
    peopleNumber: number | null;
}

export interface UserCreation {
    birthdayYear: number;
    email: string;
    gender: 'h' | 'f';
    generalTermsOfUse: boolean;
    job?: string;
    locations?: Coordinates[];
    locationShared: boolean;
    locationType?: string;
    password?: string;
    productionActivity: 'personal' | 'professional';
    productionShared: boolean;
    productionType?: ProductionUser;
    surface?: SurfaceUser;
}

export interface UserAbstract extends UserCreation {
    id: number;
    confirmed: null | boolean;
    blocked: null | boolean;
    role: RoleUser;
    createdAt: string | Date;
    updatedAt: string | Date;
}

export interface UserStrapi extends UserAbstract {
    createdAt: string;
    updatedAt: string;
}

export interface User extends UserAbstract {
    createdAt: Date;
    updatedAt: Date;
}

export interface AuthenticationResponse {
    jwt: string;
    user: UserStrapi
}
