export interface HomePageCard {
    id: number;
    title: string;
    description: string;
    illustration: {
        alternativeText: string;
        id: number;
        url: string;
    };
}

export interface HomePage {
    id: number;
    title: string;
    description: string;
    locale: string;
    cards: HomePageCard[];
    localizations: string[];
}
