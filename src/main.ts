import {createApp} from 'vue'
import App from '@/App.vue'
import router from '@/plugins/router';
import store from '@/store';
import vuetify from '@/plugins/vuetify';
import i18n from '@/plugins/i18n';

createApp(App)
    .use(router)
    .use(store)
    .use(vuetify)
    .use(i18n)
    .mount('#app')
