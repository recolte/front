class CoreService {
    setFlashMessage(name: string, message: string = ''): void {
        const flash = sessionStorage.getItem('flash')

        if (flash) {
            const messageParsed = JSON.parse(flash)
            sessionStorage.setItem('flash', JSON.stringify({...messageParsed, [name]: message === '' ? true : message}))
        } else {
            sessionStorage.setItem('flash', JSON.stringify({[name]: message === '' ? true : message}))
        }
    }

    getFlashMessage(name: string): string | boolean | undefined {
        const sessionFlash = sessionStorage.getItem('flash')

        if (sessionFlash === null) return undefined

        const flash: { [key: string]: string | boolean } = JSON.parse(sessionFlash)

        if (flash?.[name] === undefined) return undefined

        const result: string | boolean = flash?.[name]

        delete flash?.[name]

        if (Object.keys(flash)) sessionStorage.setItem('flash', JSON.stringify(flash))

        return result
    }

    uuidGenerate(): string {
        const uuidTemplate = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
        let uuid = ''

        for (const char of uuidTemplate) {
            uuid += char === 'x' ? (Math.floor(Math.random() * 16)).toString(16) : char
        }

        return uuid
    }
}

export default new CoreService()
