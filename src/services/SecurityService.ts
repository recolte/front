import {AxiosResponse} from 'axios';
import _axios from '@/plugins/axios';
import store from '@/store';
import CoreService from '@/services/CoreService';
import router from '@/plugins/router';
import {AuthenticationResponse, UserCreation, UserStrapi} from '#/Security';

class SecurityService {
    connection(payload: { identifier: string, password: string }): Promise<any> {
        return new Promise((resolve, reject) => {
            _axios.post('/auth/local', payload)
                  .then((response: AxiosResponse<AuthenticationResponse>) => {
                      const {jwt, user} = response.data
                      localStorage.setItem('JWT_ACCESS', jwt)

                      Promise.all([
                          store.dispatch('loadJWT'),
                          store.dispatch('loadMe', user),
                      ])
                             .then(() => {
                                 router.push({name: 'Vegetables'})
                                       .then(() => {
                                           resolve('')
                                       })
                             })
                  })
                  .catch(error => {
                      reject(error)
                  })
        })
    }

    loadMe(): void {
        _axios.get('/users/me')
              .then(async (response: AxiosResponse<UserStrapi>) => {
                  await store.dispatch('loadMe', response.data)
              })
    }

    testEmail(email: string): Promise<AxiosResponse> {
        return _axios.post('/users-permissions/test-email', {email})
    }

    createUser(user: UserCreation): void {
        _axios.post('/users', user)
              .then(async () => {
                  CoreService.setFlashMessage('userCreated')
                  await router.push({name: 'SignIn'})
              })
    }
}

export default new SecurityService()
