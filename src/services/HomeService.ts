import {HomePage} from '#/Home';
import _axios from '@/plugins/axios';
import {AxiosResponse} from 'axios';

class HomeService {
    loadHomeText(): Promise<HomePage> {
        return new Promise(resolve => {
            _axios.get('/home-page')
                  .then((response: AxiosResponse<HomePage>) => {
                      resolve(response.data)
                  })
        })
    }
}

export default new HomeService()
