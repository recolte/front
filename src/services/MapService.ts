import _axios from '@/plugins/axios';
import {AxiosResponse} from 'axios';
import {Coordinates, OSMReverseResponse} from '#/Map';
import {UserStrapi} from '#/Security';

class MapService {
    uuidGenerator(): string {
        const uuidTemplate = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
        let uuid = ''

        for (const char of uuidTemplate) {
            uuid += char === 'x' ? (Math.floor(Math.random() * 16)).toString(16) : char
        }

        return uuid
    }

    getNeighbors({lat, lng}: Coordinates): Promise<UserStrapi[]> {
        return new Promise(resolve => {
            _axios.get(`/users-permissions/neighbors?lat=${lat}&lng=${lng}`)
                  .then((response: AxiosResponse<UserStrapi[]>) => {
                      resolve(response.data)
                  })
        })
    }

    getAddress(lat: number, lng: number): Promise<AxiosResponse<OSMReverseResponse>> {
        return _axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=jsonv2&zoom=18&polygon=1&addressdetails=1`)
    }

    searchAddress(address: string): Promise<AxiosResponse<OSMReverseResponse[]>> {
        return _axios.get(`https://nominatim.openstreetmap.org/search?format=jsonv2&polygon=1&addressdetails=1&q=${address.split(' ')
                                                                                                                          .join('+')}`)
    }
}

export default new MapService()
