import _axios from '@/plugins/axios';
import store from '@/store';
import {AxiosResponse} from 'axios';
import {Harvest} from '#/Harvest';
import {ChartData} from '#/Map';

export interface NewHarvestPayload {
    vegetable: number;
    weight?: number;
    number?: number;
}

class HarvestService {
    loadMyHarvests(): Promise<any> {
        return new Promise<any>(resolve => {
            _axios.get('/harvests')
                  .then((response: AxiosResponse<Harvest[]>) => {
                      const data: ChartData[] = []

                      response.data.reduce((vegetablesList: ChartData[], harvest: Harvest) => {
                          const vegetableIndex = vegetablesList.findIndex(item => item.category === harvest.vegetable.name)

                          const weightAverage = harvest.vegetable.name === 'Carotte' ? 125 : 150

                          if (vegetableIndex === -1) {
                              vegetablesList.push({
                                  category: harvest.vegetable.name,
                                  y1: 0,
                                  y2: 0,
                                  y3: harvest.weight ? harvest.weight : harvest.number ? harvest.number * weightAverage : 0,
                              })
                          } else {
                              vegetablesList[vegetableIndex].y3 += harvest.weight ? harvest.weight : harvest.number ? harvest.number * weightAverage : 0
                          }

                          return vegetablesList
                      }, data)

                      resolve(data)
                  })
        })
    }

    saveNewHarvest(payload: NewHarvestPayload): Promise<void> {
        return new Promise((resolve) => {
            _axios.post('/harvests', payload)
                  .then(async () => {
                      await store.dispatch('selectVegetable', null)
                      resolve()
                  })
        })
    }
}

export default new HarvestService()
