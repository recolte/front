import _axios from '@/plugins/axios';
import {AxiosResponse} from 'axios';
import store from '@/store';
import {Vegetable} from '#/Vegetable';

class VegetableService {
    loadAllVegetables(): Promise<Vegetable[]> {
        return new Promise((resolve) => {
            _axios.get('/vegetables')
                  .then((response: AxiosResponse<Vegetable[]>) => {
                      store.dispatch('setVegetables', response.data)
                           .then(() => {
                               resolve(response.data)
                           })
                  })
        })
    }

    loadFavoritesVegetables(): void {
        _axios.get('vegetables/users')
              .then(async (response: AxiosResponse<{ vegetables: number[] }>) => {
                  await store.dispatch('setFavoriteVegetables', response.data.vegetables)
              })
    }
}

export default new VegetableService()
