import {createI18n} from 'vue-i18n'
import fr from '@/locales/fr.json'
import en from '@/locales/en.json'

let locale = <string>import.meta.env.NODE_ENV === 'test'
    ? <string>import.meta.env.VITE_APP_I18N_LOCALE || 'fr'
    : navigator.language.split('-')[0] || <string>import.meta.env.VITE_APP_I18N_LOCALE || 'fr'

export default createI18n({
    locale,
    fallbackLocale: 'fr',
    messages: {fr, en},
})
