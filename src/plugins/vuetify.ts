import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/lib/styles/main.sass'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/lib/components'
import * as directives from 'vuetify/lib/directives'

const myCustomLightTheme = {
    dark: false,
    variables: {},
    colors: {
        background: '#ffffff',
        surface: '#ffffff',
        primary: '#eb8e46',
        secondary: '#65eb68',
        success: '#8afa7c',
        warning: '#d6c65e',
        error: '#b50c09',
        info: '#865ff5',
    },
}

export default createVuetify({
    components,
    directives,
    theme: {
        defaultTheme: 'myCustomLightTheme',
        themes: {
            myCustomLightTheme,
        },
    },
})
