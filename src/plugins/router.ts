import {createRouter, createWebHistory, Router, RouteRecordRaw} from 'vue-router';
import _store from '@/store'
import CoreService from '@/services/CoreService';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        name: 'Home',
        component: () => import('@/views/Home.vue'),
    },
    {
        path: '/login',
        name: 'SignIn',
        component: () => import('@/views/Security/SignIn.vue'),
    },
    {
        path: '/signup',
        name: 'SignUp',
        component: () => import('@/views/Security/SignUp.vue'),
    },
    {
        path: '/',
        component: () => import('@/views/App/AppTemplate.vue'),
        children: [
            {
                path: 'vegetables',
                name: 'Vegetables',
                component: () => import('@/views/App/Vegetables.vue'),
                meta: {
                    connected: true,
                },
            },
            {
                path: 'data',
                name: 'Data',
                component: () => import('@/views/App/Data.vue'),
                meta: {
                    connected: true,
                },
            },
            {
                path: 'neighbors',
                name: 'Neighbors',
                component: () => import('@/views/App/Neighbors.vue'),
                meta: {
                    connected: true,
                },
            },
            {
                path: 'profile',
                name: 'Profile',
                component: () => import('@/views/App/Profile.vue'),
                meta: {
                    connected: true,
                },
            },
        ],
    },
]

const router: Router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach(((to, from, next) => {
    if ((to.name === 'SignUp' || to.name === 'SignIn') && _store.getters.userIsConnected) {
        next({name: 'Vegetables'})
    } else if (to.meta.connected && !_store.getters.userIsConnected) {
        CoreService.setFlashMessage('pageProtected')
        next({name: 'SignIn'})
    } else {
        next()
    }
}))

export default router
