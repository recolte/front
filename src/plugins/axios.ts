import axios from 'axios'

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = import.meta.env.VITE_APP_BACK_URL
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

let config = {
    baseURL: <string>import.meta.env.VITE_APP_BACK_URL,
    // timeout: 60 * 1000, // Timeout
    // withCredentials: true, // Check cross-site Access-Control
}

const _axios = axios.create(config)

const urlNotProtected = [
    '/users-permissions/test-email',
    '/auth/local',
    '/users',
    '/home-page',
]

_axios.interceptors.request.use(
    function (config) {
        // Do something before request is sent
        if (!urlNotProtected.includes(<string>config.url)) {
            config.headers.authorization = `Bearer ${localStorage.getItem('JWT_ACCESS')}`
        }
        return config
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error)
    },
)

// Add a response interceptor
_axios.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response
    },
    function (error) {
        // Do something with response error
        return Promise.reject(error)
    },
)

export default _axios
