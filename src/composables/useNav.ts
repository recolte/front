import {VueI18n} from 'vue-i18n';

export interface NavItem {
    text: string;
    icon: string;
    to: string;
}

export default function ({t}: VueI18n, isSmartphone: boolean) {
    let navItems: (NavItem | null)[] = [
        {
            text: t(`template.dashboard.links.${isSmartphone ? 'smartphone.' : ''}vegetables`),
            icon: 'mdi-carrot',
            to: 'Vegetables'
        },
        {
            text: t(`template.dashboard.links.${isSmartphone ? 'smartphone.' : ''}data`),
            icon: 'mdi-finance',
            to: 'Data'
        },
        {
            text: t(`template.dashboard.links.${isSmartphone ? 'smartphone.' : ''}neighbors`),
            icon: 'mdi-google-maps',
            to: 'Neighbors'
        },
    ]

    if (isSmartphone) {
        navItems.push({
            text: <string>t('template.dashboard.links.smartphone.profile'),
            icon: 'mdi-account',
            to: 'Profile',
        })
        navItems.splice(2, 0, null)
    }

    return {
        navItems,
    }
}
