import {useStore} from 'vuex';
import router from '@/plugins/router';
import CoreService from '@/services/CoreService';

export default function () {
    const store = useStore()

    const onLogout = (): void => {
        store.dispatch('logout')
             .then(async () => {
                 CoreService.setFlashMessage('logout')
                 window.localStorage.removeItem('JWT_ACCESS')
                 await router.push({name: 'SignIn'})
             })
    }

    return {
        onLogout,
    }
}
