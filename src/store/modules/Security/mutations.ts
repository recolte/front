import {MutationTree} from 'vuex';
import {SecurityState} from '@/store/modules/Security/state';
import {UserStrapi} from '#/Security';

export default <MutationTree<SecurityState>>{
    LOAD_JWT: (state: SecurityState, jwt: string | null) => {
        state.jwt = jwt
    },

    LOAD_ME: (state: SecurityState, me: UserStrapi) => {
        state.me = me
    },
}
