import {ActionContext, ActionTree} from 'vuex';
import {RootState} from '@/store';
import {SecurityState} from '@/store/modules/Security/state';
import {UserStrapi} from '#/Security';

export default <ActionTree<SecurityState, RootState>>{
    loadJWT: ({commit}: ActionContext<SecurityState, RootState>) => {
        commit('LOAD_JWT', localStorage.getItem('JWT_ACCESS'))
    },

    loadMe: ({commit}: ActionContext<SecurityState, RootState>, me: UserStrapi) => {
        commit('LOAD_ME', me)
    },

    logout: ({commit}: ActionContext<SecurityState, RootState>) => {
        commit('LOAD_JWT', null)
    }
}
