import {UserStrapi} from '#/Security';

export interface SecurityState {
    jwt: string | null;
    me: UserStrapi | null;
}

export default <SecurityState>{
    jwt: null,
    me: null,
}
