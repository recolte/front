import {GetterTree} from 'vuex';
import {RootState} from '@/store';
import {SecurityState} from '@/store/modules/Security/state';
import {UserStrapi} from '#/Security';

export default <GetterTree<SecurityState, RootState>>{
    me: (state: SecurityState): UserStrapi | null => state.me,
    userIsConnected: (state: SecurityState): boolean => !!state.jwt,
}
