import state from '@/store/modules/Security/state';
import actions from '@/store/modules/Security/actions';
import mutations from '@/store/modules/Security/mutations';
import getters from '@/store/modules/Security/getters';

export default {
    namespaced: false,
    state,
    actions,
    mutations,
    getters,
}
