import state from '@/store/modules/Vegetable/state';
import actions from '@/store/modules/Vegetable/actions';
import mutations from '@/store/modules/Vegetable/mutations';
import getters from '@/store/modules/Vegetable/getters';

export default {
    namespaced: false,
    state,
    actions,
    mutations,
    getters,
}
