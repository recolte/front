import {GetterTree} from 'vuex';
import {VegetableState} from '@/store/modules/Vegetable/state';
import {RootState} from '@/store';
import {Vegetable} from '#/Vegetable';

export default <GetterTree<VegetableState, RootState>>{
    favoriteVegetables: (state: VegetableState): number[] => state.favoriteVegetables,
    vegetables: (state: VegetableState): Vegetable[] => state.vegetables,
    vegetableIsSelected: (state: VegetableState): boolean => !!state.vegetableSelected,
    vegetableSelected: (state: VegetableState): Vegetable | null => state.vegetableSelected,
}
