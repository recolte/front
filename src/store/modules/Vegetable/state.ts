import {Vegetable} from '#/Vegetable';

export interface VegetableState {
    favoriteVegetables: number[];
    vegetables: Vegetable[];
    vegetableSelected: Vegetable | null;
}

export default <VegetableState>{
    favoriteVegetables: [],
    vegetables: [],
    vegetableSelected: null,
}
