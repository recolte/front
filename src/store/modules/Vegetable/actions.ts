import {ActionContext, ActionTree} from 'vuex';
import {VegetableState} from '@/store/modules/Vegetable/state';
import {RootState} from '@/store';
import {Vegetable} from '#/Vegetable';

export default <ActionTree<VegetableState, RootState>>{
    selectVegetable: ({commit}: ActionContext<VegetableState, RootState>, vegetableSelected: Vegetable | null): void => {
        commit('SELECT_VEGETABLE', vegetableSelected)
    },
    setFavoriteVegetables: ({commit}: ActionContext<VegetableState, RootState>, vegetables: number[]): void => {
        commit('SET_FAVORITE_VEGETABLES', vegetables)
    },
    setVegetables: ({commit}: ActionContext<VegetableState, RootState>, vegetables: Vegetable[]): void => {
        commit('SET_VEGETABLES', vegetables)
    },
}
