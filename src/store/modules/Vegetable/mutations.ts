import {MutationTree} from 'vuex';
import {VegetableState} from '@/store/modules/Vegetable/state';
import {Vegetable} from '#/Vegetable';

export default <MutationTree<VegetableState>>{
    SELECT_VEGETABLE: (state: VegetableState, vegetableSelected: Vegetable | null): void => {
        state.vegetableSelected = vegetableSelected
    },
    SET_FAVORITE_VEGETABLES: (state: VegetableState, vegetables: number[]): void => {
        state.favoriteVegetables.splice(0, state.favoriteVegetables.length)

        vegetables.forEach(vegetable => {
            state.favoriteVegetables.push(vegetable)
        })
    },
    SET_VEGETABLES: (state: VegetableState, vegetables: Vegetable[]): void => {
        state.vegetables.splice(0, state.vegetables.length)

        vegetables.sort((a, b) => {
            if (a.name < b.name) return -1
            else if (a.name > b.name) return 1

            return 0
        })
                  .forEach(vegetable => {
                      state.vegetables.push(vegetable)
                  })
    },
}
