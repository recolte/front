import {createStore} from 'vuex';
import Security from './modules/Security';
import Vegetable from './modules/Vegetable';
import {SecurityState} from '@/store/modules/Security/state';
import {VegetableState} from '@/store/modules/Vegetable/state';

export interface RootState {
    Security: SecurityState,
    Vegetable: VegetableState,
}

export default createStore<RootState>({
    modules: {
        Security,
        Vegetable,
    },
})
