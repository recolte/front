module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-typescript',
    [
      'babel-preset-vite',
      {
        env: true,
        glob: true,
      },
    ],
  ],
  plugins: [
    'babel-plugin-transform-import-meta',
    'transform-es2015-modules-commonjs',
    function () {
      return {
        visitor: {
          MetaProperty (path) {
            path.replaceWithSourceString('process')
          },
        },
      }
    },
  ],
}
