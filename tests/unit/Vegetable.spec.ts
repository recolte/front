import store from '@/store';
import i18n from '@/plugins/i18n';
import router from '@/plugins/router';
import axios from '@/plugins/axios';
import {mount} from '@vue/test-utils';
import Vegetables from '@/views/App/Vegetables.vue';
import VegetableForm from '@/components/App/VegetableForm.vue';
import {i18nMock, vuetify} from './utils/Plugins';
import {VegetableState} from '@/store/modules/Vegetable/state';
import moxios from 'moxios';
import {
    harvestWeightResponse,
    vegetableListStrapi,
    vegetableListStrapiSorted,
    vegetableTomato,
} from './utils/Vegetables';
import HarvestService from '../../src/services/HarvestService';
import 'regenerator-runtime/runtime.js';

const initVegetableState = (): VegetableState => ({
    favoriteVegetables: [],
    vegetableSelected: null,
    vegetables: [],
})


describe('Vegetable', () => {
    beforeEach(() => {
        store.state.Vegetable = initVegetableState()
        moxios.install(axios)
    })

    afterEach(() => {
        moxios.uninstall(axios)
    })

    describe('Vegetable list', () => {
        it('should display a vignette list when vegetable are loaded', (done) => {
            const vegetableListWrapper = mount(Vegetables, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            moxios.stubRequest('/vegetables', {
                status: 200,
                response: vegetableListStrapi(),
            })

            router.isReady()
                  .then(() => {
                      moxios.wait(() => {
                          expect(vegetableListWrapper.vm.vegetables).toEqual(vegetableListStrapiSorted())

                          expect(vegetableListWrapper.findComponent({ref: 'vignette-1'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-3'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-5'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-7'}).exists())
                              .toBe(true)

                          vegetableListWrapper.unmount()
                          done()
                      })
                  })
        });

        it('should select the associated vegetable when it clicks on vignette', (done) => {
            const vegetableListWrapper = mount(Vegetables, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            moxios.stubRequest('/vegetables', {
                status: 200,
                response: vegetableListStrapi(),
            })

            router.isReady()
                  .then(() => {
                      moxios.wait(() => {
                          expect(store.getters.vegetableSelected).toBe(null)
                          expect(store.getters.vegetableIsSelected).toBe(false)

                          vegetableListWrapper.findComponent({ref: 'vignette-1'})
                                              .find('span')
                                              .trigger('click')

                          expect(store.getters.vegetableIsSelected).toBe(true)
                          expect(store.getters.vegetableSelected)
                              .toEqual(vegetableTomato())
                          vegetableListWrapper.unmount()
                          done()
                      })
                  })
        });

        it('should filter the vegetable list when use the text field', (done) => {
            const vegetableListWrapper = mount(Vegetables, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            moxios.stubRequest('/vegetables', {
                status: 200,
                response: vegetableListStrapi(),
            })

            router.isReady()
                  .then(() => {
                      moxios.wait(() => {
                          expect(vegetableListWrapper.vm.vegetables).toEqual(vegetableListStrapiSorted())

                          expect(vegetableListWrapper.findComponent({ref: 'vignette-1'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-3'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-5'}).exists())
                              .toBe(true)
                          expect(vegetableListWrapper.findComponent({ref: 'vignette-7'}).exists())
                              .toBe(true)

                          vegetableListWrapper.find('#vegetableSearch').setValue('Tomate')
                                              .then(() => {
                                                  expect(vegetableListWrapper.vm.vegetables)
                                                      .toEqual([vegetableTomato()])

                                                  expect(
                                                      vegetableListWrapper.findComponent({ref: 'vignette-1'})
                                                                          .exists(),
                                                  )
                                                      .toBe(true)
                                                  expect(
                                                      vegetableListWrapper.findComponent({ref: 'vignette-3'})
                                                                          .exists(),
                                                  )
                                                      .toBe(false)
                                                  expect(
                                                      vegetableListWrapper.findComponent({ref: 'vignette-5'})
                                                                          .exists(),
                                                  )
                                                      .toBe(false)
                                                  expect(
                                                      vegetableListWrapper.findComponent({ref: 'vignette-7'})
                                                                          .exists(),
                                                  )
                                                      .toBe(false)

                                                  vegetableListWrapper.unmount()
                                                  done()
                                              })
                      })
                  })
        });
    })

    describe('Harvest creation', () => {
        const saveNewHarvestMethod = HarvestService.saveNewHarvest
        const saveNewHarvestSpy = jest.fn()

        beforeEach(() => {
            store.state.Vegetable.vegetableSelected = vegetableTomato()
            store.state.Vegetable.vegetables = vegetableListStrapi()

            HarvestService.saveNewHarvest = saveNewHarvestSpy.mockImplementation(saveNewHarvestMethod)
        })

        afterEach(() => {
            HarvestService.saveNewHarvest = saveNewHarvestMethod
        })

        it('should display a form with vegetable information', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(vegetableFormWrapper.vm.vegetableSelected).toEqual(vegetableTomato())
                      expect(vegetableFormWrapper.vm.vegetableUrl)
                          // @ts-ignore
                          .toBe(`${import.meta.env.VITE_APP_BACK_URL}${vegetableTomato().picture.url}`)
                      vegetableFormWrapper.unmount()
                      done()
                  })
        });

        it('should undisplay the form when it clicks on the cancel button', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(vegetableFormWrapper.vm.vegetableSelected).toEqual(vegetableTomato())

                      vegetableFormWrapper.findComponent({ref: 'cancel-button'})
                                          .trigger('click')

                      expect(vegetableFormWrapper.vm.vegetableSelected).toBe(null)

                      vegetableFormWrapper.unmount()
                      done()
                  })
        });

        it('should display a error message when it submits the form without data', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(vegetableFormWrapper.vm.isError).toBe(false)
                      expect(vegetableFormWrapper.vm.informationMessage).toBe(null)

                      vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                          .trigger('click')

                      expect(vegetableFormWrapper.vm.isError).toBe(true)
                      expect(vegetableFormWrapper.vm.informationMessage)
                          .toBe(i18nMock('vegetable.form.errors.oneValueRequired'))

                      vegetableFormWrapper.unmount()
                      done()
                  })
        });

        it('should display a error message when it submits the form with a weight without unit', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(vegetableFormWrapper.vm.isError).toBe(false)
                      expect(vegetableFormWrapper.vm.informationMessage).toBe(null)

                      vegetableFormWrapper.find('#weight')
                                          .setValue(4)
                      vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                          .trigger('click')

                      expect(vegetableFormWrapper.vm.isError).toBe(true)
                      expect(vegetableFormWrapper.vm.informationMessage)
                          .toBe(i18nMock('vegetable.form.errors.unitRequired'))

                      vegetableFormWrapper.unmount()
                      done()
                  })
        });

        it('should create a new harvest if weight defined', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            moxios.stubRequest('/harvests', {
                status: 201,
                response: harvestWeightResponse(),
            })

            router.isReady()
                  .then(() => {
                      moxios.wait(() => {
                          expect(vegetableFormWrapper.vm.isError).toBe(false)

                          vegetableFormWrapper.find('#weight')
                                              .setValue(4)
                          vegetableFormWrapper.find('#unit')
                                              .setValue('g')
                          vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                              .trigger('click')

                          expect(vegetableFormWrapper.vm.isError).toBe(false)
                          expect(saveNewHarvestSpy).toBeCalled()
                          expect(saveNewHarvestSpy).toBeCalledWith({vegetable: 1, weight: 4})

                          vegetableFormWrapper.find('#weight')
                                              .setValue(4)
                          vegetableFormWrapper.find('#unit')
                                              .setValue('kg')
                          vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                              .trigger('click')

                          expect(vegetableFormWrapper.vm.isError).toBe(false)
                          expect(saveNewHarvestSpy).toBeCalled()
                          expect(saveNewHarvestSpy).toBeCalledWith({vegetable: 1, weight: 4000})

                          vegetableFormWrapper.find('#weight')
                                              .setValue(4)
                          vegetableFormWrapper.find('#unit')
                                              .setValue('t')
                          vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                              .trigger('click')

                          expect(vegetableFormWrapper.vm.isError).toBe(false)
                          expect(saveNewHarvestSpy).toBeCalled()
                          expect(saveNewHarvestSpy).toBeCalledWith({vegetable: 1, weight: 4000000})

                          vegetableFormWrapper.unmount()
                          done()
                      })
                  })
        });

        it('should create a new harvest if number defined', (done) => {
            const vegetableFormWrapper = mount(VegetableForm, {
                global: {
                    plugins: [vuetify, store, i18n, router],
                },
            })

            moxios.stubRequest('/harvests', {
                status: 201,
                response: harvestWeightResponse(),
            })

            router.isReady()
                  .then(() => {
                      moxios.wait(() => {
                          expect(vegetableFormWrapper.vm.isError).toBe(false)

                          vegetableFormWrapper.find('#number')
                                              .setValue(10)
                          vegetableFormWrapper.findComponent({ref: 'submit-button'})
                                              .trigger('click')

                          expect(vegetableFormWrapper.vm.isError).toBe(false)
                          expect(saveNewHarvestSpy).toBeCalled()
                          expect(saveNewHarvestSpy).toBeCalledWith({vegetable: 1, number: 10})

                          vegetableFormWrapper.unmount()
                          done()
                      })
                  })
        });
    })
})
