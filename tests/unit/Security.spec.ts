import store from '@/store'
import axios from '@/plugins/axios';
import i18n from '@/plugins/i18n';
import router from '@/plugins/router';
import {vuetify, i18nMock} from './utils/Plugins';
import './utils/Storages'
import moxios from 'moxios';
import {connectionError, connectionResponse, jwt, userCreated, userStrapi} from './utils/Security';
import {mount} from '@vue/test-utils';
import SignIn from '@/views/Security/SignIn.vue';
import SignUp from '@/views/Security/SignUp.vue';
import HomeButtons from '@/components/Home/HomeButtons.vue';
import CoreService from '@/services/CoreService';
import 'regenerator-runtime/runtime.js';
import SecurityService from '@/services/SecurityService';

describe('Security', () => {
    beforeEach(() => {
        localStorage.removeItem('JWT_ACCESS')
        store.state.Security.jwt = null
        moxios.install(axios)
    })

    afterEach(() => {
        moxios.uninstall(axios)
    })

    describe('Sign in', () => {
        it('should not have jwt and user not connected when the jwt not saved in the local storage', () => {
            expect(store.getters.userIsConnected).toBe(false)
            expect(store.state.Security.jwt).toBe(null)

            store.dispatch('loadJWT')
            expect(store.getters.userIsConnected).toBe(false)
            expect(store.state.Security.jwt).toBe(null)
        })

        it('should display error message if the credentials are wrong', done => {
            expect(store.getters.userIsConnected).toBe(false)
            expect(store.state.Security.jwt).toBe(null)

            moxios.stubRequest('/auth/local', {status: 400, response: connectionError})

            const SignInWrapper = mount(SignIn, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            router.isReady()
                  .then(() => {
                      SignInWrapper.find('button').trigger('click')

                      moxios.wait(() => {
                          expect(SignInWrapper.vm.message).toBe(i18nMock('security.login.error'))
                          expect(SignInWrapper.vm.isError).toBe(true)
                          expect(store.getters.userIsConnected).toBe(false)
                          expect(store.state.Security.jwt).toBe(null)
                          SignInWrapper.unmount()
                          done()
                      })
                  })
        })

        it('should get jwt if the credentials are correct', done => {
            expect(store.getters.userIsConnected).toBe(false)
            expect(store.state.Security.jwt).toBe(null)

            moxios.stubRequest('/auth/local', {status: 200, response: connectionResponse})

            const SignInWrapper = mount(SignIn, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            router.isReady()
                  .then(() => {
                      SignInWrapper.find('button').trigger('click')

                      moxios.wait(() => {
                          expect(SignInWrapper.vm.message).toBe(i18nMock('security.login.connection'))
                          expect(SignInWrapper.vm.isError).toBe(false)
                          expect(store.getters.userIsConnected).toBe(true)
                          expect(store.state.Security.jwt).toBe(jwt)
                          SignInWrapper.unmount()
                          done()
                      })
                  })
        })

        it('should display "user created" message if the component mounted with flash message', done => {
            CoreService.setFlashMessage('userCreated')

            const SignInWrapper = mount(SignIn, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(SignInWrapper.vm.message).toBe(i18nMock('security.login.userCreated'))
                      expect(SignInWrapper.vm.isError).toBe(false)
                      SignInWrapper.unmount()
                      done()
                  })
        })

        it('should display "page protected" error message if the user would like access to protected page when it is not connected', done => {
            CoreService.setFlashMessage('pageProtected')

            const SignInWrapper = mount(SignIn, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            router.isReady()
                  .then(() => {
                      expect(SignInWrapper.vm.message).toBe(i18nMock('security.login.pageProtected'))
                      expect(SignInWrapper.vm.isError).toBe(true)
                      SignInWrapper.unmount()
                      done()
                  })
        })
    })

    describe('Sign up', () => {
        it('should be have error when submit the form without values', async () => {
            const SignUpWrapper = mount(SignUp, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            moxios.stubRequest('/users', {
                status: 201,
                response: userStrapi,
            })

            await router.isReady()
            const pushMethodSpied = jest.fn()
            const pushMethod = router.push
            router.push = pushMethodSpied
            expect(SignUpWrapper.vm.formSubmitted).toBe(false)
            expect(SignUpWrapper.vm.emailHasError).toBe(false)
            expect(SignUpWrapper.vm.passwordHasError).toBe(false)
            expect(SignUpWrapper.vm.genderHasError).toBe(false)
            expect(SignUpWrapper.vm.professionalJobHasError).toBe(false)
            expect(SignUpWrapper.vm.generalTermsOfUseHasError).toBe(false)

            await SignUpWrapper.findComponent({ref: 'submitButton'}).trigger('click')
            expect(SignUpWrapper.vm.formSubmitted).toBe(true)
            expect(SignUpWrapper.vm.emailHasError).toBe(true)
            expect(SignUpWrapper.vm.passwordHasError).toBe(true)
            expect(SignUpWrapper.vm.genderHasError).toBe(true)
            expect(SignUpWrapper.vm.professionalJobHasError).toBe(true)
            expect(SignUpWrapper.vm.generalTermsOfUseHasError).toBe(true)

            expect(CoreService.getFlashMessage('userCreated')).toBe(undefined)
            expect(pushMethodSpied).toHaveBeenCalledTimes(0)
            router.push = pushMethod
            SignUpWrapper.unmount()
        })

        // @ts-ignore
        it('should send the user data if the required values are defined', async (done) => {
            const SignUpWrapper = mount(SignUp, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            moxios.stubRequest('/users', {
                status: 201,
                response: userStrapi,
            })

            await router.isReady()
            const pushMethodSpied = jest.fn()
            const pushMethod = router.push
            router.push = pushMethodSpied
            await SignUpWrapper.find('#email').setValue('john@doe.fr')

            await SignUpWrapper.find('#password1').setValue('aB_1234578')
            await SignUpWrapper.find('#password2').setValue('aB_1234578')

            await SignUpWrapper.find('#gender_h').trigger('click')

            await SignUpWrapper.find('#professionalJob_personal').trigger('click')

            await SignUpWrapper.find('label[for="generalTermsOfUse"]').trigger('click')

            await SignUpWrapper.findComponent({ref: 'submitButton'}).trigger('click')
            expect(SignUpWrapper.vm.emailHasError).toBe(false)
            expect(SignUpWrapper.vm.passwordHasError).toBe(false)
            expect(SignUpWrapper.vm.genderHasError).toBe(false)
            expect(SignUpWrapper.vm.professionalJobHasError).toBe(false)
            expect(SignUpWrapper.vm.generalTermsOfUseHasError).toBe(false)

            moxios.wait(() => {
                expect(CoreService.getFlashMessage('userCreated')).toBe(true)
                expect(pushMethodSpied).toHaveBeenCalled()
                expect(pushMethodSpied).toHaveBeenCalledWith({name: 'SignIn'})
                router.push = pushMethod
                SignUpWrapper.unmount()
                done()
            })
        })

        it('should send the user data if the required values are defined', async () => {
            const SignUpWrapper = mount(SignUp, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            const createUserMethod = SecurityService.createUser
            const createUserMethodSpied = jest.fn()
            SecurityService.createUser = createUserMethodSpied

            await router.isReady()
            await SignUpWrapper.find('#email').setValue('john@doe.fr')

            await SignUpWrapper.find('#password1').setValue('aB_1234578')
            await SignUpWrapper.find('#password2').setValue('aB_1234578')

            await SignUpWrapper.find('#gender_h').trigger('click')

            await SignUpWrapper.find('#birthday_year').setValue('1986')

            await SignUpWrapper.find('#activity_intermediateProfession').trigger('click')

            await SignUpWrapper.find('#professionalJob_personal').trigger('click')

            await SignUpWrapper.find('#location_insideHome').trigger('click')
            await SignUpWrapper.find('#sub_location_balconyPlanter').trigger('click')

            await SignUpWrapper.find('#surfaceNumber').setValue('11')
            await SignUpWrapper.find('#surfaceUnit').setValue('m²')

            await SignUpWrapper.find('#productionType_home').trigger('click')
            await SignUpWrapper.find('#productionTypePeopleNumber').setValue('2')

            await SignUpWrapper.find('label[for="generalTermsOfUse"]').trigger('click')

            await SignUpWrapper.findComponent({ref: 'submitButton'}).trigger('click')

            expect(createUserMethodSpied).toBeCalled()
            expect(createUserMethodSpied).toBeCalledWith(userCreated)

            SecurityService.createUser = createUserMethod

            SignUpWrapper.unmount()
        })

        describe('Sign up components', () => {
            describe('Email component', () => {
                it('should have error if email not valid or empty and if the form is submitted', async () => {
                    const SignUpWrapper = mount(SignUp, {
                        global: {
                            plugins: [store, i18n, router, vuetify],
                        },
                    })

                    await router.isReady()
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.formSubmitted).toBe(false)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(false)

                    await SignUpWrapper.findComponent({ref: 'submitButton'}).trigger('click')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.formSubmitted).toBe(true)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(true)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage)
                        .toBe(i18nMock('security.signup.components.email.errors.required'))

                    await SignUpWrapper.find('#email').setValue('bad value')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('bad value')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.formSubmitted).toBe(true)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(true)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage)
                        .toBe(i18nMock('security.signup.components.email.errors.format'))

                    const testEmailMethod = SecurityService.testEmail
                    const testEmailMethodSpied = jest.fn().mockImplementation(testEmailMethod)
                    SecurityService.testEmail = testEmailMethodSpied
                    await SignUpWrapper.find('#email').setValue('john@doe.fr')
                    await SignUpWrapper.find('#email').trigger('blur')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('john@doe.fr')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.formSubmitted).toBe(true)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(false)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage).toBe('')

                    expect(testEmailMethodSpied).toBeCalled()
                    expect(testEmailMethodSpied).toBeCalledWith('john@doe.fr')
                    SecurityService.testEmail = testEmailMethod

                    SignUpWrapper.unmount()
                })

                it('should not have error if the form is not submitted', async () => {
                    const SignUpWrapper = mount(SignUp, {
                        global: {
                            plugins: [store, i18n, router, vuetify],
                        },
                    })

                    await router.isReady()
                    // @ts-ignore
                    SignUpWrapper.findComponent('#emailComponent').vm.onCheckEmail('')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(false)

                    await SignUpWrapper.find('#email').setValue('bad value')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('bad value')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(false)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage).toBe('')

                    const testEmailMethod = SecurityService.testEmail
                    const testEmailMethodSpied = jest.fn().mockImplementation(testEmailMethod)
                    SecurityService.testEmail = testEmailMethodSpied
                    await SignUpWrapper.find('#email').setValue('john@doe.fr')
                    await SignUpWrapper.find('#email').trigger('blur')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.email).toBe('john@doe.fr')
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.hasError).toBe(false)
                    // @ts-ignore
                    expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage).toBe('')

                    expect(testEmailMethodSpied).toBeCalled()
                    expect(testEmailMethodSpied).toBeCalledWith('john@doe.fr')
                    SecurityService.testEmail = testEmailMethod

                    SignUpWrapper.unmount()
                })

                // @ts-ignore
                it('should not have error if the email not used', async (done) => {
                    const SignUpWrapper = mount(SignUp, {
                        global: {
                            plugins: [store, i18n, router, vuetify],
                        },
                    })

                    moxios.stubRequest('/users-permissions/test-email', {status: 204})

                    await router.isReady()
                    // @ts-ignore
                    SignUpWrapper.findComponent('#emailComponent').vm.onCheckEmail('john@doe.fr')

                    moxios.wait(() => {
                        expect(SignUpWrapper.vm.email).toBe('john@doe.fr')
                        expect(SignUpWrapper.vm.emailHasError).toBe(false)
                        // @ts-ignore
                        expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage).toBe('')
                        SignUpWrapper.unmount()
                        done()
                    })
                })

                // @ts-ignore
                it('should have "alreadyUsed" error if the email already used and the API returned 409 status code', async (done) => {
                    const SignUpWrapper = mount(SignUp, {
                        global: {
                            plugins: [store, i18n, router, vuetify],
                        },
                    })

                    moxios.stubRequest('/users-permissions/test-email', {
                        status: 409,
                        response: {
                            statusCode: 409,
                            error: 'Conflict',
                            message: {
                                id: 'Auth.form.error.email.taken',
                                message: 'Email is already taken.',
                            },
                        },
                    })

                    await router.isReady()
                    // @ts-ignore
                    SignUpWrapper.findComponent('#emailComponent').vm.onCheckEmail('john@doe.fr')

                    moxios.wait(() => {
                        expect(SignUpWrapper.vm.email).toBe('')
                        expect(SignUpWrapper.vm.emailHasError).toBe(true)
                        // @ts-ignore
                        expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage)
                            .toBe(i18nMock('security.signup.components.email.errors.alreadyUsed'))
                        SignUpWrapper.unmount()
                        done()
                    })
                })

                // @ts-ignore
                it('should have "errorNotDefined" error if the API return other error status code', async (done) => {
                    const SignUpWrapper = mount(SignUp, {
                        global: {
                            plugins: [store, i18n, router, vuetify],
                        },
                    })

                    moxios.stubRequest('/users-permissions/test-email', {
                        status: 400,
                        response: {
                            statusCode: 400,
                            error: 'Required',
                            message: {
                                id: 'Auth.form.error.email.required',
                                message: 'Email is required.',
                            },
                        },
                    })

                    await router.isReady()
                    // @ts-ignore
                    SignUpWrapper.findComponent('#emailComponent').vm.onCheckEmail('john@doe.fr')

                    moxios.wait(() => {
                        expect(SignUpWrapper.vm.email).toBe('')
                        expect(SignUpWrapper.vm.emailHasError).toBe(true)
                        // @ts-ignore
                        expect(SignUpWrapper.findComponent('#emailComponent').vm.errorMessage)
                            .toBe(i18nMock('security.signup.components.email.errors.errorNotDefined'))
                        SignUpWrapper.unmount()
                        done()
                    })
                })
            })

            describe('Password component', () => {
                it('should have the list of option with a cross before', () => {
                    // TODO: finir test
                })
            })
        })
    })

    describe('Logout', () => {
        it('should disconnect user when it clicks on the logout button', (done) => {
            const homePageWrapper = mount(HomeButtons, {
                global: {
                    plugins: [store, i18n, router, vuetify],
                },
            })

            moxios.stubRequest('/auth/local', {status: 200, response: connectionResponse})

            const push = router.push
            const pushSpy = jest.fn().mockImplementation(push)
            router.push = pushSpy

            router.isReady()
                  .then(() => {
                      expect(window.localStorage.getItem('JWT_ACCESS')).toBe(null)
                      expect(homePageWrapper.findComponent({ref: 'signInButton'}).exists()).toBe(true)
                      expect(homePageWrapper.findComponent({ref: 'signUpButton'}).exists()).toBe(true)
                      expect(homePageWrapper.findComponent({ref: 'goAppLink'}).exists()).toBe(false)
                      expect(homePageWrapper.findComponent({ref: 'logoutButton'}).exists()).toBe(false)
                      expect(store.getters.userIsConnected).toBe(false)

                      SecurityService.connection({
                          identifier: 'JohnDoe',
                          password: '12345678',
                      })
                                     .then(() => {
                                         moxios.wait(async () => {
                                             expect(pushSpy).toBeCalledWith({name: 'Vegetables'})
                                             expect(window.localStorage.getItem('JWT_ACCESS'))
                                                 .toBe(connectionResponse.jwt)
                                             expect(window.sessionStorage.getItem('flash'))
                                                 .toBe(JSON.stringify({}))
                                             expect(homePageWrapper.findComponent({ref: 'signInButton'}).exists())
                                                 .toBe(false)
                                             expect(homePageWrapper.findComponent({ref: 'signUpButton'}).exists())
                                                 .toBe(false)
                                             expect(homePageWrapper.findComponent({ref: 'goAppLink'}).exists())
                                                 .toBe(true)
                                             expect(homePageWrapper.findComponent({ref: 'logoutButton'}).exists())
                                                 .toBe(true)
                                             expect(store.getters.userIsConnected).toBe(true)

                                             await homePageWrapper.findComponent({ref: 'logoutButton'}).trigger('click')

                                             expect(window.localStorage.getItem('JWT_ACCESS')).toBe(null)
                                             expect(window.sessionStorage.getItem('flash'))
                                                 .toBe(JSON.stringify({logout: true}))
                                             expect(pushSpy).toBeCalledWith({name: 'SignIn'})
                                             expect(homePageWrapper.findComponent({ref: 'signInButton'}).exists())
                                                 .toBe(true)
                                             expect(homePageWrapper.findComponent({ref: 'signUpButton'}).exists())
                                                 .toBe(true)
                                             expect(homePageWrapper.findComponent({ref: 'goAppLink'}).exists())
                                                 .toBe(false)
                                             expect(homePageWrapper.findComponent({ref: 'logoutButton'}).exists())
                                                 .toBe(false)
                                             expect(store.getters.userIsConnected).toBe(false)

                                             router.push = push
                                             homePageWrapper.unmount()
                                             done()
                                         })
                                     })
                  })
        });
    })
})
