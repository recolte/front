import {Vegetable} from '../../../types/Vegetable';
import {Harvest} from '../../../types/Harvest';

export const vegetableTomato = (): Vegetable => ({
    id: 1,
    name: 'Tomate',
    locale: 'fr',
    picture: {
        id: 5,
        url: '/uploads/tomate_808e57c79d.svg',
        alternativeText: '',
    },
    localizations: [
        {
            id: 2,
            locale: 'en',
        },
    ],
})


export const vegetableListStrapi = (): Vegetable[] => ([
    vegetableTomato(),
    {
        id: 3,
        name: 'Poireau',
        locale: 'fr',
        picture: {
            id: 8,
            url: '/uploads/poireau_a9d1d10c86.svg',
            alternativeText: 'Illustration de poireau',
        },
        localizations: [
            {
                id: 4,
                locale: 'en',
            },
        ],
    },
    {
        id: 5,
        name: 'Aubergine',
        locale: 'fr',
        picture: {
            id: 6,
            url: '/uploads/aubergine_4c9bc36924.svg',
            alternativeText: 'Illustration d\'aubergine',
        },
        localizations: [
            {
                id: 6,
                locale: 'en',
            },
        ],
    },
    {
        id: 7,
        name: 'Carotte',
        locale: 'fr',
        picture: {
            id: 7,
            url: '/uploads/carotte_820d113833.svg',
            alternativeText: 'Illustration de carotte',
        },
        localizations: [
            {
                id: 8,
                locale: 'en',
            },
        ],
    },
])

export const vegetableListStrapiSorted = (): Vegetable[] => vegetableListStrapi().sort((a, b) => {
    if (a.name < b.name) return -1
    else if (a.name > b.name) return 1

    return 0
})

export const harvestWeightResponse = (): Harvest => ({
    id: 1,
    number: null,
    weight: 10000,
    user: 1,
    vegetable: {
        id: 1,
        name: 'Tomate',
        picture: {
            id: 5,
            url: '/uploads/tomate_808e57c79d.svg',
            alternativeText: '',
        },
    },
})
