import {AuthenticationResponse, UserCreation, UserStrapi} from '../../../types/Security';

export const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'

export const userStrapi: UserStrapi = {
    id: 7,
    email: 'jane@doe.fr',
    confirmed: true,
    blocked: false,
    role: {
        id: 1,
        name: 'Authenticated',
        type: 'authenticated',
    },
    createdAt: '2021-08-05T14:55:12.826Z',
    updatedAt: '2021-08-14T08:13:14.818Z',
    gender: 'h',
    productionActivity: 'personal',
    birthdayYear: 1986,
    job: 'Employé',
    locationType: 'À mon domicile - jardinière balcon',
    locations: [
        {
            lat: 48.5691199,
            lng: 7.7789912,
            surface: 11,
            unit: 'm'
        },
    ],
    productionType: {
        value: 'home',
        peopleNumber: 2,
    },
    productionShared: true,
    locationShared: true,
    generalTermsOfUse: true,
}

export const connectionResponse: AuthenticationResponse = {
    jwt,
    user: userStrapi,
}

export const connectionError = {
    statusCode: 400,
    error: 'Bad Request',
    message: [
        {
            messages: [
                {
                    id: 'Auth.form.error.email.provide',
                    message: 'Please provide your username or your e-mail.',
                },
            ],
        },
    ],
    data: [
        {
            messages: [
                {
                    id: 'Auth.form.error.email.provide',
                    message: 'Please provide your username or your e-mail.',
                },
            ],
        },
    ],
}

export const userCreated: UserCreation = {
    gender: 'h',
    email: 'john@doe.fr',
    password: 'aB_1234578',
    birthdayYear: 1986,
    productionActivity: 'personal',
    productionShared: false,
    locationShared: false,
    job: 'intermediateProfession',
    locationType: 'insideHome - balconyPlanter',
    surface: {value: 11, unit: 'm²'},
    productionType: {value: 'home', peopleNumber: 2},
    generalTermsOfUse: true,
}

