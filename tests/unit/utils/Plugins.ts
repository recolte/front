import {createVuetify} from 'vuetify';
import fr from '@/locales/fr.json'

export const vuetify = createVuetify()

interface Locales {
    [key: string]: string | Locales
}

export const i18nMock = (path: string): string => {
    const keys = path.split('.')
    return <string>keys.reduce<Locales | string>((previousValue: Locales | string, currentValue: string) => {
        if (typeof previousValue === 'string') {
            return previousValue
        }

        return previousValue[currentValue]
    }, fr)
}
